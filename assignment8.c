#include<stdio.h>
#include<string.h>
//This program will record and display student data (name,subject,marks)
//T.H.H.Abeywardana-2019/IS/002

struct students{
	char name[15];
	char subject[15];
	int marks;
};

struct students s[10];
int x;

//fetches the data of the students
void fetchData();

//prints the data of the students
void printData();

void fetchData(){
	for(x=0;x<10;x++){
		printf("Enter student first name: ");
		scanf("%s",s[x].name);
		
		printf("Enter the subject: ");
		scanf("%s",s[x].subject);
		
		printf("Enter marks: ");
		scanf("%d",&s[x].marks);
		
		printf("\n");
	}
}

void printData(){
	printf("+-----------------+-----------------+-------+\n");
	printf("| name            | subject         | marks |\n");
	for(x=0;x<10;x++){
		printf("+-----------------+-----------------+-------+\n");
		printf("| %-15s | %-15s | %5d |\n",s[x].name,s[x].subject,s[x].marks);
	}
	printf("+-----------------+-----------------+-------+\n");
}

int main(){
	fetchData();
	printData();
	
	return 0;
}
